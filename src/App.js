import Header from "./components/HeaderComponent/Index";
import TopMainSliderComponent from "./components/TopMainSliderComponent/Index";
import TopMainSliderItem from "./components/TopMainSliderComponent/TopMainSliderItem/Index";
import CenterTextComponent from "./components/CenterTextComponent/Index";
import VideoSectionComponent from "./components/VideoSectionComponent/Index";
import SurfboardsComponent from "./components/SurfboardsComponent/Index";
import SurfboardsItem from "./components/SurfboardsComponent/SurfboardsItem/Index";
import StepBoxStyleOneComponent from "./components/StepBoxStyleOneComponent/Index";
import StepBoxStyleTwoComponent from "./components/StepBoxStyleTwoComponent/Index";
import ContactComponent from "./components/ContactComponent/Index";
import SideBarComponent from "./components/SideBarComponent/Index";
import PeopleSectionComponent from "./components/PeopleSectionComponent/Index";
import FooterComponent from "./components/FooterComponent/Index";
import "./style.scss"
import 'bootstrap/dist/css/bootstrap.min.css';
import "./assets/font/spartan/style.scss"
import "./assets/font/Poppins/style.scss"
import "./assets/font/Playfair/style.scss"

import board1 from "./assets/img/board1.png"
import board2 from "./assets/img/board2.png"
import board3 from "./assets/img/board3.png"
import Aos from "aos"
import React from 'react';
import 'aos/dist/aos.css';

function App() {
    React.useEffect(()=>{

        setTimeout(()=>{
            Aos.init();
        },1000)
    })
    return (
        <div className="App"   >

            <div className="container bg">
                <Header/>
                <TopMainSliderComponent>
                    <TopMainSliderItem/>
                    <TopMainSliderItem/>
                    <TopMainSliderItem/>
                </TopMainSliderComponent>
                <CenterTextComponent/>
                <VideoSectionComponent/>
            </div>
            <div>
                <SurfboardsComponent>
                    <SurfboardsItem image={board1} type={"Funboards"} title={"Chilli Rare Bird"} price={"890"}/>
                    <SurfboardsItem image={board2} type={"Funboards"} title={"Emery NEM XF"} price={"890"}/>
                    <SurfboardsItem image={board3} type={"Funboards"} title={"Agency GROM"} price={"890"}/>

                    <SurfboardsItem image={board1} type={"Funboards"} title={"Chilli Rare Bird"} price={"890"}/>
                    <SurfboardsItem image={board2} type={"Funboards"} title={"Emery NEM XF"} price={"890"}/>
                    <SurfboardsItem image={board3} type={"Funboards"} title={"Agency GROM"} price={"890"}/>

                </SurfboardsComponent>
            </div>

            <div className="container bg">
                <StepBoxStyleOneComponent/>
                <StepBoxStyleTwoComponent/>
            </div>
            <ContactComponent/>
            <PeopleSectionComponent/>
            <FooterComponent/>


            <SideBarComponent/>
        </div>
    );
}

export default App;
