import React from 'react';
import "./style.scss"
import img_one from "../../assets/img/58f27ac22dbc6714f26af8a4_surf-camp.jpg"

function Index() {
    return (
        <section className="StepBoxStyleTwoComponent-section overflow-hidden">
            <article className="both-top-and-bottom container w-container">
                <div className="row">
                    <div className="box-right col-12 col-md-6" data-aos='fade-right' data-aos-duration={700} >
                        <span className="text-image-left">
                            <img alt="Point Break" className="small-image"
                                 src={img_one}
                                 width="244.5"/>
                        </span>
                    </div>
                    <div className="box-left  col-12  col-md-6" data-aos='fade-left' data-aos-duration={700}>
                        <div className="heading-block" data-ix="float-in-on-scroll">
                            <div className="number">02</div>
                            <h2 className="sub-heading">Point Break</h2>
                            <div className="small-hr"/>
                        </div>
                        <div className="d-flex">
                            <p className="text-bottom col-12 col-md-5">By
                                better understanding the various aspects of By better understanding the various aspects
                                of
                                surfing, By better understanding the various aspects of surfing, you will improve faster
                                and
                                have more fun in the water.</p>
                        </div>
                        <a className="text-link-more"
                           href="#">R&nbsp; e&nbsp; a&nbsp; d &nbsp;&nbsp; M&nbsp; o&nbsp; r&nbsp; e</a>
                    </div>
                </div>
            </article>
        </section>
    )
}

export default Index;