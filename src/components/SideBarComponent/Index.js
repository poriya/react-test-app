import React from 'react';
import "./style.scss"

function Index() {
    return (

          <div className="side-bar">
              <p className="side-bar__text">First Surfing Magazine</p>
              <div className="social-block">
                  <a className="social-icon d-inline-block" href="#">
                      <img className="tumble"
                           src="https://daks2k3a4ib2z.cloudfront.net/58f24ff59590171867ff6fe6/58f296602dbc6714f26afe2d_tumblr.svg"
                           width="41"/>
                  </a>
                  <a className="social-icon d-inline-block" href="#">
                      <img
                          src="https://daks2k3a4ib2z.cloudfront.net/58f24ff59590171867ff6fe6/58f296511be878187a8095f2_twitter.svg"
                          width="41"/>
                  </a>
                  <a className="social-icon d-inline-block" href="#">
                      <img
                          src="https://daks2k3a4ib2z.cloudfront.net/58f24ff59590171867ff6fe6/58f2960fc2933c49669c1561_vimeo.svg"
                          width="41"/>
                  </a>
              </div>
          </div>

    )
}

export default Index;