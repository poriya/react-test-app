import React from 'react';
import "./style.scss"
import logo from "../../assets/img/surfboard-logo.svg"
import tumblr from "../../assets/img/tumblr.svg"
import twitter from "../../assets/img/twitter.svg"
import vimeo from "../../assets/img/vimeo.svg"
function Index() {
    return (
      <footer className="FooterComponent" data-aos='zoom-in' data-aos-duration={700}>
          <div className="container inside-bg">
              <div className="row">
                  <div className="col-12 text-center logo-image">
                      <img src={logo} alt="logo" width="15px"/>
                  </div>
                  <div className="col-12 d-flex align-items-center justify-content-center flex-wrap footer-links">
                      <a href="#">Stories</a>
                      <a href="#">Events</a>
                      <a href="#">Places</a>
                      <a href="#">Boards</a>
                  </div>
                  <div className="col-12 d-flex align-items-center justify-content-center flex-wrap footer-links pt-4 pb-5">
                      <a href="#"><img src={tumblr} alt="tumblr" width="25px"/></a>
                      <a href="#"><img src={twitter} alt="tumblr" width="25px"/></a>
                      <a href="#"><img src={vimeo} alt="tumblr" width="25px"/></a>

                  </div>

              </div>
          </div>
      </footer>
    )
}

export default Index;