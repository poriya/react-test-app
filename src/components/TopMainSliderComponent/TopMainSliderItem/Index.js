import React from 'react';
import "./style.scss"
import slider_image from '../../../assets/img/top-slider-image.png'
function Index() {
    return (
        <div className="topMainSliderItem">
            <img src={slider_image} alt="top-slider" className="top-slider-image"  data-aos='zoom-in' data-aos-duration={700} />
            <div className="content-data" data-aos='fade-up' data-aos-duration={700}>
                <div className="textTop">
                    Your
                </div>
                <div className="text-title">
                    Beautiful Escape
                </div>
                <div className="text-description">
                    <div className="col-6   col-md-4">
                        One of the greatest things about the sport of surfing is that you need only three things: your body,
                        a surfboard, and a wave.
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Index;