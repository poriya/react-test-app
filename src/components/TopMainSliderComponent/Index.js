import React, {useEffect, useState} from 'react';
import "./style.scss"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function Index(props) {
    const sliderRef = React.createRef()
    const [count, setCount] = useState(0)
    const [index, setIndex] = useState(0)

    let settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        afterChange: (i) => {
            setIndex(i)
        }
    };
    useEffect(() => {
        setCount(props.children.length)
    }, [props.children])

    return (
        <section className="TopMainSliderComponent">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <Slider ref={sliderRef} {...settings} >
                            {props.children}
                        </Slider>
                        <div className="d-flex align-items-center">
                            <span className="arrow"  onClick={() => {
                                sliderRef.current.slickNext()


                            }}>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="11px" height="18px">
                                    <path fillRule="evenodd" fill="rgb(235, 235, 235)"
                                          d="M0.154,8.640 L9.106,0.149 C9.317,0.50 9.655,0.50 9.865,0.149 C10.76,0.349 10.76,0.670 9.865,0.869 L1.296,8.998 L9.865,17.128 C10.76,17.327 10.76,17.648 9.865,17.848 C9.762,17.946 9.623,17.999 9.488,17.999 C9.352,17.999 9.213,17.950 9.110,17.848 L0.158,9.357 C0.51,9.161 0.51,8.836 0.154,8.640 Z"/>
                                    </svg>
                            </span>
                            <span className="indexSlider">
                                <span className="text-blue">{index+1}</span>/{count}</span>
                            <span className="arrow"  onClick={() => {
                                sliderRef.current.slickPrev()

                            }}>
                                   <svg
                                       xmlns="http://www.w3.org/2000/svg"

                                       width="11px" height="18px">
                                <path fillRule="evenodd" fill="rgb(235, 235, 235)"
                                      d="M9.845,8.640 L0.921,0.149 C0.711,0.50 0.374,0.50 0.164,0.149 C0.45,0.349 0.45,0.670 0.164,0.869 L8.707,8.998 L0.164,17.128 C0.45,17.327 0.45,17.648 0.164,17.848 C0.267,17.946 0.406,17.999 0.541,17.999 C0.676,17.999 0.814,17.950 0.917,17.848 L9.841,9.357 C10.51,9.161 10.51,8.836 9.845,8.640 Z"/>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Index;