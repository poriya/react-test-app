import React from 'react';
import "./style.scss"

function Index() {
    return (
      <section className="ContactComponent-section">
          <div className="container inside-bg">
              <div className="row justify-content-center flex-column">

                  <div className="col-12 py-5 my-4 text-center John_McCarthy"  data-aos='zoom-in' data-aos-duration={700} >
                      <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="2px" height="100px">
                          <path fillRule="evenodd"  fill="rgb(93, 168, 239)"
                                d="M0.0,0.0 L1.999,0.0 L1.999,99.999 L0.0,99.999 L0.0,0.0 Z"/>
                      </svg>
                  </div>
                  <div className="col-12 text-center title pb-5"  data-aos='zoom-in' data-aos-duration={700}>
                      Join the Club
                  </div>
                  <div className="col-12 text-center description pb-5"  data-aos='zoom-in' data-aos-duration={700}>
                      By better understanding the various aspects of surfing, you will improve faster
                      <br/>
                      and have more fun in the water.
                  </div>
              </div>
              <div className="row">
                  <div className="col"  data-aos='zoom-in' data-aos-duration={700}>
                      <form action="/" className="contact-form justify-content-center">
                          <div className="col-12 col-lg-6 d-flex align-items-center">
                              <input type="text" placeholder="Your E-mail" className="col-10"/>
                              <button className="col-2">
                                  <svg
                                      xmlns="http://www.w3.org/2000/svg"

                                      width="50px" height="50px">
                                      <path fillRule="evenodd"  fill="rgb(93, 168, 239)"
                                            d="M0.0,0.0 L49.999,0.0 L49.999,49.999 L0.0,49.999 L0.0,0.0 Z"/>
                                      <path fillRule="evenodd"  fill="rgb(255, 255, 255)"
                                            d="M29.845,24.640 L20.921,16.149 C20.711,15.949 20.374,15.949 20.164,16.149 C19.954,16.349 19.954,16.669 20.164,16.869 L28.707,24.999 L20.164,33.128 C19.954,33.328 19.954,33.649 20.164,33.848 C20.267,33.946 20.406,33.999 20.541,33.999 C20.676,33.999 20.814,33.950 20.917,33.848 L29.841,25.357 C30.51,25.161 30.51,24.836 29.845,24.640 Z"/>
                                  </svg>

                              </button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </section>
    )
}

export default Index;