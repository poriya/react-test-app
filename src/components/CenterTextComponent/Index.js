import React from 'react';
import "./style.scss"

function Index() {
    return (
      <section className="CenterTextComponent-section">
          <div className="container">
              <div className="row justify-content-center">
                  <div className="col-12 col-md-5 text-center" data-aos='fade-up' data-aos-duration={700}>
                      Surfing is the most blissful experience you can have

                      on this planet, a taste of heaven.
                  </div>
                  <div className="col-12 pt-3 text-center John_McCarthy" data-aos='fade-up' data-aos-duration={700}>
                      John McCarthy
                  </div>
                  <div className="col-12 py-3 text-center John_McCarthy" data-aos='fade-up' data-aos-duration={700}>
                      <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="2px" height="100px">
                          <path fillRule="evenodd"  fill="rgb(93, 168, 239)"
                                d="M0.0,0.0 L1.999,0.0 L1.999,99.999 L0.0,99.999 L0.0,0.0 Z"/>
                      </svg>
                  </div>
              </div>
          </div>
      </section>
    )
}

export default Index;