import React from 'react';
import "./style.scss"
import img_one from "../../assets/img/58f27ac2a5352543aeedc61b_surf-splash.jpg"

function Index() {
    return (
        <section className="StepBoxStyleOneComponent-section">
            <div className="container">
                <div className="row">
                    <div className="box-left order-lg-0 order-1  col-12 col-md-5"  data-aos='zoom-in' data-aos-duration={1000}>
                        <div className="heading-block">
                            <div className="number">01</div>
                            <h2 className="sub-heading">Surfboards</h2>
                            <div className="small-hr"/>
                        </div>
                        <p className="text-bottom">By
                            better understanding the various aspects of surfing, By better understanding the various
                            aspects of surfing, you will improve faster and have more fun in the water.</p>
                        <a className="text-link-more"
                           href="#">R&nbsp; e&nbsp; a&nbsp; d &nbsp;&nbsp; M&nbsp; o&nbsp; r&nbsp; e</a>
                    </div>
                    <div className="box-right order-lg-1 order-0  col-12 col-md-7" data-aos='zoom-in' data-aos-duration={900}>
                        <img alt="Surfboards" src={img_one}

                        />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Index;