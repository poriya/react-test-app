import React from 'react';
import "./style.scss"
import img_one from "../../assets/img/58f27ac2d5fc681355abd914_surfer-2.jpg"
import img_two from "../../assets/img/58f27ac290a1b8136563f5ce_surfer-rocks.jpg"
function Index() {
    return (
        <section className="VideoSectionComponent">
            <div className="container">
                <div className="row"  >
                    <div className="box-right col col-6 col-sm-6"  data-aos='zoom-in' data-aos-duration={700} >
                        <img className="left-image" alt="Surfer on the Rocks" src={img_two}

                        />

                    </div>
                    <div className="box-left col col-6 col-sm-6"  data-aos='zoom-in' data-aos-duration={700} >
                        <a className="right-image"   href="#" >
                            <img  alt="Surfer Video Thumbnail" src={img_one}

                            />
                                <div className="play-button">
                                    <div className="play-icon">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"

                                            width="14px" height="17px">
                                            <path fillRule="evenodd"  fill="rgb(255, 255, 255)"
                                                  d="M13.709,8.473 L0.348,16.187 L0.348,0.759 L13.709,8.473 Z"/>
                                        </svg>
                                    </div>
                                </div>

                        </a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-lg-6 bottom-text">
                        <div className="padded-text-container"
                        >
                            <p className="description">By better understanding the various aspects of surfing, you will
                                improve faster and have more fun in the water.</p>
                            <a className="text-link" href="#">R&nbsp; e&nbsp; a&nbsp; d &nbsp;&nbsp; M&nbsp; o&nbsp; r&nbsp; e</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Index;