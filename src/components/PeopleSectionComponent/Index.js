import React from 'react';
import "./style.scss"
import img_people from "../../assets/img/img-people.png"
function Index() {
    return (
      <section className="PeopleSectionComponent">
          <div className="container inside-bg">
              <div className="row">
                  <div className="col-12 col-md-6" data-aos='zoom-in' data-aos-duration={700}>
                      <img src={img_people} alt="img_people" className="img_people"/>
                  </div>
                  <div className="col-12 col-md-6 d-flex   flex-column justify-content-center">
                      <div className="Our_Camp" data-aos='zoom-in' data-aos-duration={700}>Our Camp</div>
                      <div className="Silver_Strand_" data-aos='zoom-in' data-aos-duration={700}>
                          CA 91932, USA
                          <br/>
                          Imperial Beach
                          <br/>
                          560 Silver Strand Blvd

                      </div>
                      <div className="get_t" data-aos='zoom-in' data-aos-duration={700}>
                          <a href="#" className="myLink">
                              Get in Touch

                          </a>
                      </div>

                  </div>
              </div>
          </div>
      </section>
    )
}

export default Index;