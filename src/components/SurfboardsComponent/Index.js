import React, { useEffect} from 'react';
import "./style.scss"
import Slider from "react-slick";

function Index(props) {

    const sliderRef = React.createRef()


    let settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,

                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,

                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
        // afterChange: (i) => {
        //     // setIndex(i)
        // }
    };
    useEffect(() => {
        // setCount(props.children.length)
    }, [props.children])

    return (
        <section className="SurfboardsComponent-section" >
            <div className="container inside-bg">
                <div className="row"   data-aos='zoom-in' data-aos-duration={1000}  >
                    <div className="col text-center"  >
                        <div className="Shop">Shop</div>
                        <div className="Surfboards">Surfboards</div>

                    </div>
                </div>
                <div className="row">
                    <div className="col position-relative">
                        <Slider ref={sliderRef} {...settings} >
                            {props.children}
                        </Slider>
                        <div className="d-flex align-items-center" onClick={() => {
                            sliderRef.current.slickNext()


                        }}>
                            <span className="arrow left">
                                <svg xmlns="http://www.w3.org/2000/svg" width="11px" height="18px">
                                      <path fillRule="evenodd" fill="rgb(51, 51, 51)"
                                            d="M0.154,8.640 L9.106,0.149 C9.317,0.50 9.655,0.50 9.865,0.149 C10.76,0.349 10.76,0.670 9.865,0.869 L1.296,8.999 L9.865,17.128 C10.76,17.328 10.76,17.648 9.865,17.848 C9.762,17.946 9.623,17.999 9.488,17.999 C9.352,17.999 9.213,17.950 9.110,17.848 L0.158,9.357 C0.52,9.161 0.52,8.836 0.154,8.640 Z"/>
                                </svg>
                            </span>

                            <span className="arrow right" onClick={() => {
                                sliderRef.current.slickPrev()

                            }}>
                                  <svg xmlns="http://www.w3.org/2000/svg" width="11px" height="18px">
                                      <path fillRule="evenodd" fill="rgb(51, 51, 51)"
                                          d="M9.844,8.640 L0.893,0.149 C0.682,0.50 0.344,0.50 0.133,0.149 C0.76,0.349 0.76,0.670 0.133,0.869 L8.703,8.999 L0.133,17.128 C0.76,17.328 0.76,17.648 0.133,17.848 C0.237,17.946 0.376,17.999 0.511,17.999 C0.646,17.999 0.786,17.950 0.889,17.848 L9.841,9.357 C10.51,9.161 10.51,8.836 9.844,8.640 Z"/>
                                  </svg>
                            </span>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col text-center">
                        <a href="#" className="AllLink">
                            S&nbsp;h&nbsp;o&nbsp;w&nbsp;&nbsp; A&nbsp;l&nbsp;l
                        </a>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default Index;