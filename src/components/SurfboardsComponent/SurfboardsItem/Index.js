import React from 'react';
import "./style.scss"
import PropTypes from 'prop-types';

function Index(props) {
    return (
        <div className="SurfboardsItem">
            <div className="image_container" data-aos='zoom-in' data-aos-duration={700}>
                <img src={props.image} alt="Surfboard"/>
            </div>
            <div className="data_content">
                <div className="type">
                    {props.type}
                </div>
                <div className="title">
                    {props.title}
                </div>
                <div className="price pt-2">
                    <span className="d-flex flex-column">
                          <span>
                             {props.price}$
                          </span>
                          <span className="buy">
                              buy
                          </span>
                    </span>


                </div>
            </div>

        </div>
    )
}

Index.prototype = {
    image: PropTypes.String,
    buy: PropTypes.String,
    title: PropTypes.String,
}
export default Index;

