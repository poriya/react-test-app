import React from 'react';
import "./style.scss"
import logo from "../../assets/img/surfboard-logo.svg"
import menu from "../../assets/img/menu.png"
import Drawer from '@mui/material/Drawer';


function Index() {

    const [state, setState] = React.useState(false);

    return (
        <header className="headerComponent">
            <div>

                    <React.Fragment>
                        <Drawer
                            anchor={"left"}
                            open={state}
                            onClose={()=>{
                                setState(false)
                            }}>
                            <div className="d-flex flex-column" style={{width:"200px"}}>
                                <a className="nav-link" href="#">Stories</a>
                                <a className="nav-link" href="#">Events</a>
                                <a className="nav-link" href="#">Places</a>
                                <a className="nav-link" href="#">Boards</a>
                            </div>
                        </Drawer>
                    </React.Fragment>

            </div>
            <div className="navbar">
                <header className="container">
                   <div className="d-flex align-items-center w-100 responsive-menu">
                       <a className="header-nav-brand" href="/">
                           <img alt="Logo" className="logo"  src={logo} width="50"/>
                       </a>
                       <nav className="header-nav-menu d-none d-lg-flex">
                           <a className="nav-link" href="#stories">Stories</a>
                           <a className="nav-link" href="#">Events</a>
                           <a className="nav-link" href="#">Places</a>
                           <a className="nav-link" href="#surfboards">Boards</a>
                       </nav>

                       <div className="menu-button d-flex d-md-none" onClick={()=>{
                           setState(true)
                       }}>
                           <img src={menu} alt="menu"/>
                       </div>
                   </div>

                </header>
            </div>
        </header>
    )
}

export default Index;